package shapess;
//Amy Nguyen
import static org.junit.Assert.*;
import org.junit.Test;

public class SphereTest {
    @Test
    public void testSphereConstructorTrue(){
        Sphere s = new Sphere(5);
        assertEquals(5, s.getRadius(), 0.0001);
    }

    @Test
    public void testSphereConstructorWrong(){
        try{
            Sphere s = new Sphere(-6);

            fail("Negative radius, but no excpetion was thrown");
        }
        catch(IllegalArgumentException e){
            //pass
        }
    }

    @Test
    public void getVolumeRight(){
        Sphere s = new Sphere(3);
        assertEquals(113.0973, s.getVolume(), 0.0001);
    }
    
    @Test
    public void getSurfaceAreaRight(){
        Sphere s = new Sphere(9);
        assertEquals(1017.87602, s.getSurfaceArea(), 0.0001);
    }

    @Test
    public void getDiameter(){
        Sphere s = new Sphere(7);
        assertEquals(14, s.getDiameter(), 0.0001);
    }
}
