package shapess;
/* Bianca Rossetti - 2233420 */
import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class CylinderTest{
    public final double TOLERANCE = 0.001;

    @Test
    public void constructorHeight_shouldReturn4()
    {
        Cylinder c = new Cylinder(5,  4);
        assertEquals(4, c.getHeight(), TOLERANCE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_negativeRadius()
    {
        Cylinder c = new Cylinder(-5, 4);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_negativeHeight()
    {
        Cylinder c = new Cylinder(5, -4);
    }

    @Test
    public void constructorRadius_shouldReturn5()
    {
        Cylinder c = new Cylinder(5,  4);
        assertEquals(5, c.getRadius(), TOLERANCE);
    }

    @Test
    public void getVolume_shouldReturn314()
    {
        Cylinder c = new Cylinder(5,  4);
        assertEquals(314.15927, c.getVolume(), TOLERANCE);
    }

    @Test
    public void getSurfaceArea_shouldReturn282()
    {
        Cylinder c = new Cylinder(5,  4);
        assertEquals(282.74334, c.getSurfaceArea(), TOLERANCE);
    }
    
}