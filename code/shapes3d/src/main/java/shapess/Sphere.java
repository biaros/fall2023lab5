package shapess;

// Bianca Rossetti - 2233420

public class Sphere implements Shape3d{
    private double radius;
    /**
     * this contructor creates a Sphere object
     * @param double - takes value of radius
     * @return Sphere object
     */
    public Sphere(double radius)
    {
        if (radius <=0)
        {
            throw new IllegalArgumentException("Radius cannot be negative or zero!");
        }
        this.radius = radius;
    }

    /**
     * this constructor creates a copy of the Sphere object
     * @param Sphere - Sphere object to be copied
     * @return Sphere object
     */
    public Sphere(Sphere other)
    {
        this(other.radius);
    }

    /**
     * this method returns the surface area of the Sphere
     * @return value of the volume of a sphere
     */
    public double getVolume()
    {
        return((4.0/3.0)*Math.PI*Math.pow(this.radius, 3));
    }

    /**
     * this method calculates the surface area of a sphere
     * @return the value of the surface area  of a sphere
     */
    public double getSurfaceArea()
    {
        return(4.0*Math.PI*Math.pow(this.radius, 2));
    }

    /**
     * this method gets the value of the radius
     * @return value of radius
     */
    public double getRadius()
    {
        return(this.radius);
    }

    /**
     * this method gets the value of the diameter
     * @return radius * 2 (diameter calculation)
     */
    public double getDiameter()
    {
        return(this.radius*2);
    }
}
