package shapess;
//Amy Nguyen

public class Cylinder implements Shape3d {
    private double radius;
    private double height;
 
    /**
     * This is a constructor. Assignes field from params, radius and height
     * @param radius - the cylinder's radius.
     * @param height - the cylinder's height.
     */
    public Cylinder(double radius, double height){
        if(radius < 0 || height < 0){
            throw new IllegalArgumentException("Radius and/or height cannot be negative");
        }
        this.radius = radius;
        this.height = height;
    }


    /**
     * This method returns volume of cyliner.
     *@return - returns double representing the cylinder's volume 
     */
    public double getVolume(){
        return (Math.PI*(Math.pow(this.radius,2)*this.height));
    }

    /**
     * This method returns the surface area of the cylinder.
     * @return - returns double representing cylinder's surface area.
     */
    public double getSurfaceArea(){
        return(2.0*Math.PI)*Math.pow(this.radius,2.0)+(2.0*(Math.PI*this.radius*this.height));
    }

    /**
     * This method returns the radius of the cylinder.
     * @return - returns double representing the radius of the cylinder.
     */
    public double getRadius(){
        return this.radius;
    }

    /**
     * This method returns the height of the cylinder.
     * @return - returns the double representing the height of the cylinder.
     */
    public double getHeight(){
        return this.height;
    }

}
